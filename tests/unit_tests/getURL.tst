// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - Sylvestre LEDRU
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================

//getURL

myFile=getURL("http://sylvestre.ledru.info");
if myFile <> "index.html" then pause,end
inf=fileinfo(myFile);
if inf(1) == 0 then pause, end

myFile=getURL("http://sylvestre.ledru.info/");
if myFile <> "index.html" then pause,end
inf=fileinfo(myFile);
if inf(1) == 0 then pause, end


myFile=getURL("ftp://ftp.free.fr/pub/Distributions_Linux/debian/README");

if myFile <> "README" then pause,end
inf=fileinfo(myFile);
if inf(1) == 0 then pause, end


targetFile=TMPDIR+"/README_Debian";
myFile=getURL("ftp://ftp.free.fr/pub/Distributions_Linux/debian/README",targetFile);

if myFile <> targetFile then pause,end
inf=fileinfo(targetFile);
if inf(1) == 0 then pause, end
f1 = mopen(targetFile,"r");
if grep(mgetl(f1),"Linux") == [] then pause, end
mclose(f1);


targetFile="README";
myFile=getURL("ftp://ftp.free.fr/pub/Distributions_Linux/debian/README",TMPDIR);

if myFile <> TMPDIR+"/"+targetFile then pause,end
inf=fileinfo(TMPDIR+"/"+targetFile);
if inf(1) == 0 then pause, end
f1 = mopen(TMPDIR+"/"+targetFile,"r");
if grep(mgetl(f1),"Linux") == [] then pause, end
mclose(f1);

// HTTPS
myFile=getURL("https://encrypted.google.com")
if myFile <> "index.html" then pause,end
inf=fileinfo(myFile);
if inf(1) == 0 then pause, end

targetFile=TMPDIR+"/testauth";
myFile=getURL("http://httpbin.org/basic-auth/user/passwd",targetFile,"user","passwd");

if myFile <> targetFile then pause,end
inf=fileinfo(targetFile);
if inf(1) == 0 then pause, end
f1 = mopen(targetFile,"r");
if grep(mgetl(f1),"authenticated") == [] then pause, end
mclose(f1);

	// Badly formated URL
if ~execstr("getURL(''http://plop@ae:sylvestre.ledru.info:80'')",'errcatch')<>0 then pause, end

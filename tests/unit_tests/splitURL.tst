// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - Sylvestre LEDRU
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================

//splitURL

[a,b,c,d]=splitURL("http://www.scilab.org");
if a <> "http" then pause,end
if b <> "www.scilab.org" then pause,end
if c <> "" then pause,end
if d <> "" then pause,end

[a,b,c,d]=splitURL("http://www.scilab.org/");
if a <> "http" then pause,end
if b <> "www.scilab.org" then pause,end
if c <> "/" then pause,end
if d <> "" then pause,end

[a,b,c,d]=splitURL("http://www.scilab.org/products/scilab/environment");
if a <> "http" then pause,end
if b <> "www.scilab.org" then pause,end
if c <> "/products/scilab/environment" then pause,end
if d <> "" then pause,end

[a,b,c,d]=splitURL("http://www.scilab.org/content/search?SearchText=plot");
if a <> "http" then pause,end
if b <> "www.scilab.org" then pause,end
if c <> "/content/search" then pause,end
if d <> "SearchText=plot" then pause,end

[a,b,c,d]=splitURL("ftp://ftp.free.fr/pub/Distributions_Linux/debian/README");
if a <> "ftp" then pause,end
if b <> "ftp.free.fr" then pause,end
if c <> "/pub/Distributions_Linux/debian/README" then pause,end
if d <> "" then pause,end

[a,b,c,d]=splitURL("https://encrypted.google.com");
if a <> "https" then pause,end
if b <> "encrypted.google.com" then pause,end
if c <> "" then pause,end
if d <> "" then pause,end

[a,b,c,d,e,f,g]=splitURL("https://plop:ae@encrypted.google.com:443/full/path?query=true#myFragment")
if a <> "https" then pause,end
if b <> "encrypted.google.com" then pause,end
if c <> "/full/path" then pause,end
if d <> "query=true" then pause,end
if e <> "plop:ae" then pause,end
if f <> 443 then pause,end // port
if g <> "myFragment" then pause,end // fragment

// Badly formated URL
if ~execstr("splitURL(''http://plop@ae:sylvestre.ledru.info:80'');",'errcatch')<>0 then pause, end
